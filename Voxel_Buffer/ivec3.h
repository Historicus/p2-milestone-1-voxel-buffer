#pragma once

/**
 * This small struct just exists to make integer indices more compact for returning values or passing into functions.
 * 
 * CDB
 */
 
struct ivec3 {
	int x;
	int y;
	int z;
};