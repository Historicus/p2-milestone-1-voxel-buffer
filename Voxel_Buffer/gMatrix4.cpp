/**********************************************
* Author: Samuel Kenney 2/1/2017
*
* Grove City College: Introduction to Graphics
*
* Dr. Boatright
*
* Implements Matrix Operation Library class
*
**********************************************/
#include "gMatrix4.h"
#include <iostream>
#include <cmath>


#define PI 3.14159265
///----------------------------------------------------------------------
/// Constructors
///----------------------------------------------------------------------
/// Default Constructor.  Initialize to matrix of all 0s.
gMatrix4::gMatrix4(){
	gVector4 row0(0.0f, 0.0f, 0.0f, 0.0f);
	gVector4 row1(0.0f, 0.0f, 0.0f, 0.0f);
	gVector4 row2(0.0f, 0.0f, 0.0f, 0.0f);
	gVector4 row3(0.0f, 0.0f, 0.0f, 0.0f);

	data[0] = row0;
	data[1] = row1;
	data[2] = row2;
	data[3] = row3;
}

/// Initializes matrix with each vector representing a row in the matrix
gMatrix4::gMatrix4(const gVector4& row0, const gVector4& row1, const gVector4& row2, const gVector4& row3){
	data[0] = row0;
	data[1] = row1;
	data[2] = row2;
	data[3] = row3;
}

///----------------------------------------------------------------------
/// Getters
///----------------------------------------------------------------------	
/// Returns the values of the row at the index
gVector4 gMatrix4::operator[](unsigned int index) const{
	return data[index];
}

/// Returns a reference to the row at the index
gVector4& gMatrix4::operator[](unsigned int index){
	return data[index];
}

/// Returns a column of the matrix
gVector4 gMatrix4::getColumn(unsigned int index) const{
	gVector4 column;
	column[0] = data[0].operator[](index);
	column[1] = data[1].operator[](index);
	column[2] = data[2].operator[](index);
	column[3] = data[3].operator[](index);

	return column;
}

/// Prints the matrix to standard output in a nice format
void gMatrix4::print(){
	for (int i = 0; i < 4; i++){
		data[i].print();
	}
	std::cout << std::endl;
}

///----------------------------------------------------------------------
/// Matrix Operations
///----------------------------------------------------------------------	
/// Returns the transpose of the matrix (v_ij == v_ji)
gMatrix4 gMatrix4::transpose() const{
	gMatrix4 trans;
	trans[0] = getColumn(0);
	trans[1] = getColumn(1);
	trans[2] = getColumn(2);
	trans[3] = getColumn(3);

	return trans;
}

///----------------------------------------------------------------------
/// Static Initializers
///----------------------------------------------------------------------	
/// Creates a 3-D rotation matrix.
/// Takes an angle in degrees and outputs a 4x4 rotation matrix for rotating around the x-axis
gMatrix4 gMatrix4::rotationX(float angle){
	float cosTheta;
	float sineTheta;
	float negsineTheta;
	if (angle == 0){
		cosTheta = std::cos((angle * ( PI / 180 )));
		sineTheta = 0;
		negsineTheta = 0;
	} else if(angle == 90){
		cosTheta = 0;
		sineTheta = std::sin((angle * ( PI / 180 )));
		negsineTheta = -std::sin((angle * ( PI / 180 )));
	} else if (angle == 180){
		cosTheta = -1;
		sineTheta = 0;
		negsineTheta = 0;
	} else if (angle == 270){
		cosTheta = 0;
		sineTheta = -1;
		negsineTheta = 1;
	} else if (angle == 360){
		cosTheta = std::cos((angle * ( PI / 180 )));
		sineTheta = 0;
		negsineTheta = 0;
	} else {
		cosTheta = std::cos((angle * ( PI / 180 )));
		sineTheta = std::sin((angle * ( PI / 180 )));
		negsineTheta = -std::sin((angle * ( PI / 180 )));
	}


	gVector4 row0(1.0f, 0.0f, 0.0f, 0.0f), row1(0.0f, cosTheta, negsineTheta, 0.0f), row2(0.0f, sineTheta, cosTheta, 0.0f), row3(0.0f, 0.0f, 0.0f, 1.0f);
	gMatrix4 rotX(row0, row1, row2, row3);

	return rotX;
}

/// Takes an angle in degrees and outputs a 4x4 rotation matrix for rotating around the y-axis
gMatrix4 gMatrix4::rotationY(float angle){
	float cosTheta;
	float sineTheta;
	float negsineTheta;
	if (angle == 0){
		cosTheta = std::cos((angle * ( PI / 180 )));
		sineTheta = 0;
		negsineTheta = 0;
	} else if(angle == 90){
		cosTheta = 0;
		sineTheta = std::sin((angle * ( PI / 180 )));
		negsineTheta = -std::sin((angle * ( PI / 180 )));
	} else if (angle == 180){
		cosTheta = -1;
		sineTheta = 0;
		negsineTheta = 0;
	} else if (angle == 270){
		cosTheta = 0;
		sineTheta = -1;
		negsineTheta = 1;
	} else if (angle == 360){
		cosTheta = std::cos((angle * ( PI / 180 )));
		sineTheta = 0;
		negsineTheta = 0;
	} else {
		cosTheta = std::cos((angle * ( PI / 180 )));
		sineTheta = std::sin((angle * ( PI / 180 )));
		negsineTheta = -std::sin((angle * ( PI / 180 )));
	}

	gVector4 row0(cosTheta, 0.0f, sineTheta, 0.0f), row1(0.0f, 1.0f, 0.0f, 0.0f), row2(negsineTheta, 0.0f, cosTheta, 0.0f), row3(0.0f, 0.0f, 0.0f, 1.0f);
	gMatrix4 rotY(row0, row1, row2, row3);

	return rotY;
}

/// Takes an angle in degrees and outputs a 4x4 rotation matrix for rotating around the z-axis
gMatrix4 gMatrix4::rotationZ(float angle){
	float cosTheta;
	float sineTheta;
	float negsineTheta;
	if (angle == 0){
		cosTheta = std::cos((angle * ( PI / 180 )));
		sineTheta = 0;
		negsineTheta = 0;
	} else if(angle == 90){
		cosTheta = 0;
		sineTheta = std::sin((angle * ( PI / 180 )));
		negsineTheta = -std::sin((angle * ( PI / 180 )));
	} else if (angle == 180){
		cosTheta = -1;
		sineTheta = 0;
		negsineTheta = 0;
	} else if (angle == 270){
		cosTheta = 0;
		sineTheta = -1;
		negsineTheta = 1;
	} else if (angle == 360){
		cosTheta = std::cos((angle * ( PI / 180 )));
		sineTheta = 0;
		negsineTheta = 0;
	} else {
		cosTheta = std::cos((angle * ( PI / 180 )));
		sineTheta = std::sin((angle * ( PI / 180 )));
		negsineTheta = -std::sin((angle * ( PI / 180 )));
	}

	gVector4 row0(cosTheta, negsineTheta, 0.0f, 0.0f), row1(sineTheta, cosTheta, 0.0f, 0.0f), row2(0.0f, 0.0f, 1.0f, 0.0f), row3(0.0f, 0.0f, 0.0f, 1.0f);
	gMatrix4 rotZ(row0, row1, row2, row3);

	return rotZ;
}

/// Takes an x, y, and z displacement and outputs a 4x4 translation matrix
gMatrix4 gMatrix4::translation2D(float x, float y, float z){
	gVector4 row0(1.0f, 0.0f, 0.0f, x), row1(0.0f, 1.0f, 0.0f, y), row2(0.0f, 0.0f, 1.0f, z), row3(0.0f, 0.0f, 0.0f, 1.0f);
	gMatrix4 dispTran2D(row0, row1, row2, row3);

	return dispTran2D;
}

/// Takes an x, y, and z scale and outputs a 4x4 scale matrix
gMatrix4 gMatrix4::scale2D(float x, float y, float z){
	gVector4 row0(x, 0.0f, 0.0f, 0.0f), row1(0.0f, y, 0.0f, 0.0f), row2(0.0f, 0.0f, z, 0.0f), row3(0.0f, 0.0f, 0.0f, 1.0f);
	gMatrix4 scaleTran2D(row0, row1, row2, row3);

	return scaleTran2D;
}

/// Generates a 4x4 identity matrix
gMatrix4 gMatrix4::identity(){
	gMatrix4 idnty;
	gVector4 row0(1.0f, 0.0f, 0.0f, 0.0f);
	gVector4 row1(0.0f, 1.0f, 0.0f, 0.0f);
	gVector4 row2(0.0f, 0.0f, 1.0f, 0.0f);
	gVector4 row3(0.0f, 0.0f, 0.0f, 1.0f);

	idnty[0] = row0;
	idnty[1] = row1;
	idnty[2] = row2;
	idnty[3] = row3;

	return idnty;
}


///----------------------------------------------------------------------
/// Friend Functions
///----------------------------------------------------------------------
/// Checks if m1 == m2
bool gMatrix4::operator==(const gMatrix4& m2){
	int equal = 0; //Do I need to check w since it will always be the same?
	if (data[0] == m2.data[0]){equal++;}
	if (data[1] == m2.data[1]){equal++;}
	if (data[2] == m2.data[2]){equal++;}
	if (data[3] == m2.data[3]){equal++;}

	if (equal == 4){return true;}
	else {return false;}
}

/// Checks if m1 != m2
bool gMatrix4::operator!=(const gMatrix4& m2){
	int equal = 4; //Do I need to check w since it will always be the same?
	if (data[0] != m2.data[0]){equal--;}
	if (data[1] != m2.data[1]){equal--;}
	if (data[2] != m2.data[2]){equal--;}
	if (data[3] != m2.data[3]){equal--;}

	if (equal < 4){return true;}
	else {return false;}
}

/// Matrix addition (m1 + m2)
gMatrix4 gMatrix4::operator+(const gMatrix4& m2){
	gMatrix4 add;
	add[0] = data[0] + m2.data[0];
	add[1] = data[1] + m2.data[1];
	add[2] = data[2] + m2.data[2];
	//will set [i][3] to 1 or 0. Manually set correct values below
	add[3] = data[3] + m2.data[3];

	//take care of w for matrix while allowing vector or point math stay constant
	add[0][3] = data[0][3] + m2.data[0][3];
	add[1][3] = data[1][3] + m2.data[1][3];
	add[2][3] = data[2][3] + m2.data[2][3];
	add[3][3] = data[3][3] + m2.data[3][3];

	return add;
}

/// Matrix subtraction (m1 - m2)
gMatrix4 gMatrix4::operator-(const gMatrix4& m2){
	gMatrix4 sub;
	sub[0] = data[0] - m2.data[0];
	sub[1] = data[1] - m2.data[1];
	sub[2] = data[2] - m2.data[2];
	//will set [i][3] to 1 or 0. Manually set correct values below
	sub[3] = data[3] - m2.data[3];

	//take care of w for matrix while allowing vector or point math stay constant
	sub[0][3] = data[0][3] - m2.data[0][3];
	sub[1][3] = data[1][3] - m2.data[1][3];
	sub[2][3] = data[2][3] - m2.data[2][3];
	sub[3][3] = data[3][3] - m2.data[3][3];

	return sub;
}

/// Matrix multiplication (m1 * m2)
gMatrix4 gMatrix4::operator*(const gMatrix4& m2){
	gVector4 row0((data[0].operator*(m2.getColumn(0))), (data[0].operator*(m2.getColumn(1))), (data[0].operator*(m2.getColumn(2))), (data[0].operator*(m2.getColumn(3)))), 
		row1(( data[1].operator*(m2.getColumn(0))), ( data[1].operator*(m2.getColumn(1))), ( data[1].operator*(m2.getColumn(2))), ( data[1].operator*(m2.getColumn(3)))), 
		row2(( data[2].operator*(m2.getColumn(0))), ( data[2].operator*(m2.getColumn(1))), ( data[2].operator*(m2.getColumn(2))), ( data[2].operator*(m2.getColumn(3)))), 
		row3(( data[3].operator*(m2.getColumn(0))), ( data[3].operator*(m2.getColumn(1))), ( data[3].operator*(m2.getColumn(2))), ( data[3].operator*(m2.getColumn(3))));
	gMatrix4 matrixMult(row0, row1, row2, row3);

	return matrixMult;
}

/// Matrix/vector multiplication (m * v)
/// Assume v is a column vector (ie. a 4x1 matrix)
gVector4 gMatrix4::operator*(const gVector4& v){
	gVector4 vectMult( (data[0].operator*(v)), (data[1].operator*(v)), (data[2].operator*(v)), (data[3].operator*(v)) );
	return vectMult;
}


