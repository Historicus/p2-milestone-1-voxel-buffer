/**********************************************
* Author: Samuel Kenney 2/8/2017
*
* Grove City College: Introduction to Graphics
*
* Dr. Boatright
*
* Implements Voxel Buffer class
*
**********************************************/

#pragma once

#include "VoxelBuffer.h"

//constructor for Voxel Buffer class
VoxelBuffer::VoxelBuffer(float delta, const ivec3& dimensions){
	voxelBuffer = NULL;
	this->delta = delta;
	xyzCoords = dimensions;
}

//destructor for Voxel Buffer class
VoxelBuffer::~VoxelBuffer(void){
	delete [] voxelBuffer;
}

//grabs density value when given coordinates
float VoxelBuffer::densityRead(const gVector4& coords) const{
	int i, j, k, index;
	i = coords[0]/this->delta;
	j = coords[1]/this->delta;
	k = coords[2]/this->delta;
	//index = k * width    *  height    +   j * width      + i;
	index = (k * xyzCoords.x * xyzCoords.y)  + (j * xyzCoords.x) + i;

	return voxelBuffer[index].first;
}

//grabs light value when given coordinates
float VoxelBuffer::lightRead(const gVector4& coords) const{
	int i, j, k, index;
	i = coords[0]/this->delta;
	j = coords[1]/this->delta;
	k = coords[2]/this->delta;
	//index = k * width    *  height    +   j * width      + i;
	index = (k * xyzCoords.x * xyzCoords.y)  + (j * xyzCoords.x) + i;

	return voxelBuffer[index].second;
}

//writes new density value when given coordinates
void VoxelBuffer::densityWrite(const gVector4& coords, float value){
	int i, j, k, index;
	i = coords[0]/this->delta;
	j = coords[1]/this->delta;
	k = coords[2]/this->delta;
	//index = k * width    *  height    +   j * width      + i;
	index = (k * xyzCoords.x * xyzCoords.y)  + (j * xyzCoords.x) + i;


	voxelBuffer[index].first = value;
}

//writes new light value when given coordinates
void VoxelBuffer::lightWrite(const gVector4& coords, float value){
	int i, j, k, index;
	i = coords[0]/this->delta;
	j = coords[1]/this->delta;
	k = coords[2]/this->delta;
	//index = k * width    *  height    +   j * width      + i;
	index = (k * xyzCoords.x * xyzCoords.y)  + (j * xyzCoords.x) + i;

	voxelBuffer[index].second = value;
}

//returns coordinates of center of the voxel specified with coordinates
gVector4 VoxelBuffer::getVoxelCenter(const gVector4& coords) const{

	//get indices from coordniates and then add half of delta to get to the center of the voxel
	int i, j, k;
	i = coords[0]/this->delta;
	j = coords[1]/this->delta;
	k = coords[2]/this->delta;
	
	float xCoord = i*this->delta + this->delta/2;
	float yCoord = j*this->delta + this->delta/2;
	float zCoord = k*this->delta + this->delta/2;
	gVector4 coordinates(xCoord, yCoord, zCoord, 1.0f);
	return coordinates;
}

//returns coordinates of center of the voxel specified with indices
gVector4 VoxelBuffer::getVoxelCenter(const ivec3& coords) const{

	//get indices and then add half of delta to get to the center of the voxel
	float xCoord = coords.x*this->delta + this->delta/2;
	float yCoord = coords.y*this->delta + this->delta/2;
	float zCoord = coords.z*this->delta + this->delta/2;

	gVector4 coordinates(xCoord, yCoord, zCoord, 1.0f);
	return coordinates;
}

//factory for creating Voxel Buffer with 1D array
VoxelBuffer* VoxelBuffer::factory(const std::string& filename){

	//used to holding strings from text
	std::string value;
	//stream for text file
	std::ifstream voxelFile (filename);
	//will hold delta value
	float delt = 0.0;
	//used when parsing text file to hold XYZ value
	std::string coordValue;
	//will be used to store XYZC values
	ivec3 sizeCoords;
	//will be used to dynamically create a new Voxel Buffer Object
	VoxelBuffer *newVoxelBuff;
	//Index to for place in array
	int voxelIn = 0;

	//Index for place in text file
	int in = 0;
	while (getline(voxelFile, value)){
		if (in <2){
			//remove beginning text
			value.erase(0,5);
			//if DELT value convert to float
			if (in == 0){
				delt = std::stof(value);
			}
			//if XYZC value
			if (in == 1){
				//grab X
				coordValue = value.substr(0, value.find(" "));
				//remove X from string
				value.erase(0, value.find(" ")+1);
				//store X
				sizeCoords.x = std::stoi(coordValue);

				//grab Y
				coordValue = value.substr(0, value.find(" "));
				//remove Y from string
				value.erase(0, value.find(" ")+1);
				//store Y
				sizeCoords.y = std::stoi(coordValue);

				//grab Z
				coordValue = value.substr(0, value.find(" "));
				//remove Z from string
				value.erase(0, value.find(" ")+1);
				//store Z
				sizeCoords.z = std::stoi(coordValue);

			}

		//if at density values in text file
		} else {
			//if first density value
			if (in == 2){
				//create new VoxelBuffer with previous read in delta value and XYZ coordinates
				newVoxelBuff = new VoxelBuffer(delt, sizeCoords);
				//dynamically allocate space for array to hold density value and light value
				int size = sizeCoords.x * sizeCoords.y * sizeCoords.z;
				newVoxelBuff->voxelBuffer = new std::pair<float, float>[size];
			}
			//read in density and light values
			newVoxelBuff->voxelBuffer[voxelIn] = std::make_pair(std::stof(value), -1.0f);
			voxelIn++;
		}

		in++;
	}

	voxelFile.close();
	
	return newVoxelBuff;
}